package Metodo3;

import java.util.Scanner;

public class Metodo3 {
//Programa para que te muestre todas las cadenas introducidas al poner fin
	public static Scanner input = new Scanner(System.in);

		public static void main(String[] args) {
			String cadena = leeCadenas();
			System.out.println(cadena);
			input.close();
		}

		private static String leeCadenas() {
			String cadenaResultado = "";
			String cadenaLeida;
			
			do{
				System.out.println("Introduce una cadena");
				cadenaLeida = input.nextLine();
				cadenaResultado += cadenaLeida + ", ";
			}while(!cadenaLeida.equals("fin"));
			
		
			
			return cadenaResultado;
		}
		
	}



package metodo2;

import java.util.Scanner;

public class Metodo2 {

public static Scanner input = new Scanner(System.in);
//Metodo para leer todos los caracteres que introduzcas
	public static void main(String[] args) {
		String cadena = leeCaracteres();
		System.out.println(cadena);
		input.close();
	}

	private static String leeCaracteres() {
		String cadena = "";
		char caracter;
		
		
		do{
			System.out.println("Introduce un caracter, el programa terminara cuando el caracter sea 0");
			caracter = input.nextLine().charAt(0);
			cadena += caracter + " ";
		}while(caracter != '0');
		
		
		return cadena;
	}


}


